---
layout: job_page
title: "Global Recruiter"
---

## Responsibilities

* Collaborate with hiring managers to understand their requirements and set a proper recruiting strategy
* Develop accurate job descriptions to attract a qualified candidate pool
* Find creative and strategic ways to source top talent
* Promote our remote only culture
* Apply effective recruiting practices to passive and active candidates
* Screen, interview and evaluate candidates
* Assess candidate interest and ability to thrive in an open source culture
* Foster lasting relationships with candidates
* Build an effective network of internal and external resources to call on as needed
* Share best practice interviewing techniques with management team
* Ensure applicant tracking system is maintained and applicants receive timely responses
* Partner with Marketing to develop our Employer Brand in the marketplace


## Requirements

* 3+ years experience recruiting for technical roles, preferably in a technical company
* Experience with global job markets highly preferred
* Comfortable competing for top talent in a competitive global market
* Focused on delivering an excellent candidate experience
* Ambitious, efficient and stable under tight deadlines and competing priorities
* Advanced working knowledge using applicant tracking systems effectively
* Outstanding communication across all levels
* College degree from an accredited institution preferred, equivalent work experience considered
